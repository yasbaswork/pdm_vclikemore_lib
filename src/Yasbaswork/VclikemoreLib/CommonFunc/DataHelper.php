<?php

namespace Yasbaswork\VclikemoreLib\CommonFunc;

class DataHelper
{
    public static function getRandomArticles($repo, $limit=null)
    {
        // Get articles
        return $repo
            ->createQueryBuilder('a')
            ->addSelect('RAND() as HIDDEN rand')
            ->where('a.type = 0 AND a.custom = 0 AND a.article <> \'\'')
            ->addOrderBy('rand')
            ->setMaxResults($limit != null ? $limit : 1000)
            ->getQuery()
            ->getResult();
    }
}